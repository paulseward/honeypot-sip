FROM python:2.7
ADD sip-honeypot.py /
ADD creds.json /
RUN pip install google-cloud-logging
EXPOSE 5060
ENV GOOGLE_APPLICATION_CREDENTIALS /creds.json
CMD ["python", "./sip-honeypot.py"]
