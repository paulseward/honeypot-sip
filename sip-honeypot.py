#!/usr/bin/env python

import socket
import logging
from mimetools import Message
from StringIO import StringIO
from google.cloud import logging
from pprint import pprint
from datetime import datetime
import re

UDP_IP = "0.0.0.0"
UDP_PORT = 5060
USER_AGENT = 'HPv0.1.1'

# Utility function for parsing the SIP packet
def sip_parse(data):
  input_stream = StringIO(data)

  # Treat METHOD+URI+PROTO on the first line as additional headers
  methodre = re.match("([A-Z]+) ([^ ]+) (.*)$", input_stream.readline().strip())

  # If we couldn't determine the METHOD - bail out
  if not methodre:
    print "no matching METHOD"
    return {'method': 'UNKNOWN'}

  sip_request = {}
  sip_request['method'] = methodre.group(1)
  sip_request['uri'] = methodre.group(2)
  sip_request['protocol'] = methodre.group(3)
  sip_request['headers'] = {}

  # Unpack rest of the message headers
  message = Message(input_stream)
  for key in message.keys():
    sip_request['headers'][key]=message[key]
  return sip_request

def sip_200(sip_request):
  response = 'SIP/2.0 200 OK\n'
  response_headers = {
    'To': sip_request['headers'].get('to',''),
    'From': sip_request['headers'].get('from',''),
    'Allow': 'INVITE, ACK, BYE, CANCEL, OPTIONS, MESSAGE, SUBSCRIBE, NOTIFY, INFO',
    'Call-ID': sip_request['headers'].get('call-id',''),
    'CSeq': sip_request['headers'].get('cseq',''),
    'Via': '%s;received=%s' % (sip_request['headers'].get('via','').replace(';rport',''), sip_request['udp_src_ip']),
    'User-Agent': USER_AGENT,
  }
  for key in response_headers:
    response += '%s: %s\n' % (key, response_headers[key])
  return response

def sip_501(sip_request):
  response = 'SIP/2.0 501 Not Implemented\n'
  response_headers = {
    'User-Agent': USER_AGENT,
  }
  for key in response_headers:
    response += '%s: %s\n' % (key, response_headers[key])
  return response

def sip_respond(sip_request):
  if (sip_request['method'] == 'INVITE'):        # INVITE = Establishes a session.
    return sip_200(sip_request)
  elif (sip_request['method'] == 'ACK'):         # ACK = Confirms an INVITE request.
    return
  elif (sip_request['method'] == 'PRACK'):       # PRACK = Provisional Acknowledgement.
    return
  elif (sip_request['method'] == 'BYE'):         # BYE = Ends a session.
    return sip_200(sip_request)
  elif (sip_request['method'] == 'CANCEL'):      # CANCEL = Cancels establishing of a session.
    return sip_200(sip_request)
  elif (sip_request['method'] == 'REGISTER'):    # REGISTER = Communicates user location (host name, IP).
    return sip_200(sip_request)
  elif (sip_request['method'] == 'OPTIONS'):     # OPTIONS = Communicates information about the capabilities of the calling and receiving SIP phones.
    return sip_200(sip_request)
  elif (sip_request['method'] == 'SUBSCRIBE'):   # SUBSCRIBE = Subscribes for Notification from the notifier.
    return sip_501(sip_request)
  elif (sip_request['method'] == 'NOTIFY'):      # NOTIFY = Notifies the subscriber of a new event.
    return sip_501(sip_request)
  elif (sip_request['method'] == 'PUBLISH'):     # PUBLISH = Publishes an event to the Server.
    return sip_501(sip_request)
  elif (sip_request['method'] == 'INFO'):        # INFO = Sends mid session information.
    return sip_501(sip_request)
  elif (sip_request['method'] == 'REFER'):       # REFER = Asks the recipient to issue call transfer.
    return sip_501(sip_request)
  elif (sip_request['method'] == 'MESSAGE'):     # MESSAGE = Transports Instant Messages.
    return sip_501(sip_request)
  elif (sip_request['method'] == 'UPDATE'):      # UPDATE = Modifies the state of a session.
    return sip_501(sip_request)
  elif (sip_request['method'] == 'UNKNOWN'):     # Fake method returned if we couldn't identify the method
    return None
  else:
    return sip_501(sip_request)


def main():
  # Connect to the socket
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  sock.bind((UDP_IP, UDP_PORT))
 
  logging_client = logging.Client()
  logger = logging_client.logger('sip-logs')

  while True:
    data, addr = sock.recvfrom(1460) # set buffer to 1460 which our target platform MTU 

    sip_request = sip_parse(data)
    sip_request['packet']=data
    sip_request['udp_src_ip']=addr[0]
    sip_request['udp_src_port']=str(addr[1])

    # Send response - TODO
    sip_response = sip_respond(sip_request)
    if sip_response:
      sock.sendto(sip_response, addr)
    
    # Add summary message for log
    sip_request['message']="%s packet received from %s:%s" % (sip_request['method'],addr[0],addr[1])

    # Log the request
    logger.log_struct(sip_request)

    # Debugging
    print(datetime.now())
    print(addr)
    print "---------------------"
    print(data)
    print "---------------------"
    print(sip_response)
    print "====================="

if __name__ == '__main__':
 main()
