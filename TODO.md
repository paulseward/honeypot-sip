# Design:

## Initial SIP honeypot from
* https://github.com/fabio-d/honeypot/blob/master/udp_sip.py

## SIP Methods
From https://www.3cx.com/pbx/sip-methods/
```
INVITE = Establishes a session.
ACK = Confirms an INVITE request.
BYE = Ends a session.
CANCEL = Cancels establishing of a session.
REGISTER = Communicates user location (host name, IP).
OPTIONS = Communicates information about the capabilities of the calling and receiving SIP phones.
PRACK = Provisional Acknowledgement.
SUBSCRIBE = Subscribes for Notification from the notifier.
NOTIFY = Notifies the subscriber of a new event.
PUBLISH = Publishes an event to the Server.
INFO = Sends mid session information.
REFER = Asks the recipient to issue call transfer.
MESSAGE = Transports Instant Messages.
UPDATE = Modifies the state of a session.
```

SIP Responses:
SIP Requests are answered with SIP responses, of which there are six classes:
```
1xx = Informational responses, such as 180 (ringing).
2xx = Success responses.
3xx = Redirection responses.
4XX = Request failures.
5xx = Server errors.
6xx = Global failures.
```

## Architecture

* Docker container, running honeypot
* Log the following fields
  * TIMESTAMP
  * SRC IP
  * DST IP
  * METHOD
  * USER-AGENT
  * SIP-URI
  * SIP-TO
* Log via fluentd to Bigquery: https://github.com/kaizenplatform/fluent-plugin-bigquery
  See also https://cloud.google.com/solutions/real-time/fluentd-bigquery
* Could just log straight to stackdriver: https://cloud.google.com/logging/docs/setup/python
